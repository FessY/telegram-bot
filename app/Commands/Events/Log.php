<?php

namespace App\Commands\Events;

use App\Commands\BaseCommands;
use App\Commands\Command;
use App\User;
use Carbon\Carbon;

class Log extends BaseCommands implements Command
{
    /**
     * @param $response
     * @param User $user
     */
    public function runCommand($response, User $user = null)
    {
        $user->logging()->create([
            'message_id' => $response->message->message_id,
            'update_id' => $response->update_id,
            'text' => $response->message->text,
            'date' => Carbon::createFromTimestamp($response->message->date),
        ]);
    }
}