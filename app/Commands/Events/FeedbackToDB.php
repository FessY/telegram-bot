<?php

namespace App\Commands\Events;

use App\Commands\BaseCommands;
use App\Commands\Command;
use App\User;

class FeedbackToDB extends BaseCommands implements Command
{
    /**
     * @param $response
     * @param User $user
     * @throws \Exception
     */
    public function runCommand($response, User $user = null)
    {
        $lastLog = $user->logging()->orderBy('date', 'desc')->first();

        if (!is_null($lastLog) && $lastLog->text === '/feedback') {
            $this->telegram->sendMessage(
                $response->message->chat->id,
                "Спасибо, Ваше сообщение отправлено!"
            );
        }
    }
}