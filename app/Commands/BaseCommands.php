<?php

namespace App\Commands;

use App\Commands\Active\Feedback;
use App\Commands\Active\Help;
use App\Commands\Active\Off;
use App\Commands\Active\Start;
use App\Telegram\Telegram;
use App\User;

class BaseCommands
{
    protected static $command;

    protected $telegram;

    protected $commands = [
        Start::class,
        Help::class,
        Off::class,
        Feedback::class,
    ];

    public function __construct(Telegram $telegram)
    {
        $this->telegram = $telegram;
    }

    public function runCommands($response, User $user = null)
    {
        foreach ($this->commands as $command) {
            if ($response->message->text === $command::$command) {
                app($command)->runCommand($response, $user);
            }
        }
    }
}