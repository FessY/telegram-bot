<?php

namespace App\Commands;

interface Command
{
    public function runCommand($response);
}