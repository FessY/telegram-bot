<?php

namespace App\Commands;

use App\Commands\Events\CreateUser;
use App\Commands\Events\FeedbackToDB;
use App\Commands\Events\Log;
use App\Telegram\Telegram;
use App\User;

class BaseEventCommands
{
    protected $telegram;

    protected $commands = [
        FeedbackToDB::class,
        Log::class,
    ];

    public function __construct(Telegram $telegram)
    {
        $this->telegram = $telegram;
    }

    public function runCommands($response, User $user = null)
    {
        if (!is_null($user)) {
            foreach ($this->commands as $command) {
                app($command)->runCommand($response, $user);
            }
        }
    }
}