<?php

namespace App\Commands\Active;

use App\Commands\BaseCommands;
use App\Commands\Command;
class Feedback extends BaseCommands implements Command
{
    public static $command = '/feedback';
    public static $description = 'Обратная связь';

    /**
     * @param $response
     * @return mixed
     * @throws \Exception
     */
    public function runCommand($response)
    {
        return $this->telegram->sendMessage(
            $response->message->chat->id,
            'Напишите ваши пожелания или наши недочеты :)'
        );
    }
}