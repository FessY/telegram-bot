<?php

namespace App\Commands\Active;

use App\Commands\BaseCommands;
use App\Commands\Command;
use App\Jobs\SendMessage;
use App\User;
use Carbon\Carbon;
use Queue;

class Start extends BaseCommands implements Command
{
    public static $command = '/start';
    public static $description = 'Start';

    /**
     * @param object $response
     * @param User|null $user
     * @throws \Exception
     * @throws \Throwable
     */
    public function runCommand($response, User $user = null)
    {
        if (is_null($user)) {
            $createdUser = User::create([
                'chat_id' => $response->message->chat->id,
                'user_id' => $response->message->from->id,
                'username' => $response->message->from->username,
                'first_name' => $response->message->from->first_name,
                'last_name' => $response->message->from->last_name,
                'status' => 1,
            ]);

            $this->telegram->sendMessage(
                $response->message->chat->id,
                'Привет! Я бот :) Я отправляю каждый день английские слова для запоминания) При поддержке <a href="https://eng-professor.ru">https://eng-professor.ru</a>'
            );

            $this->telegram->sendMessage(
                $response->message->chat->id,
                'Первое слово для запоминания:'
            );

            Queue::push(
                (new SendMessage($createdUser, true))->delay(Carbon::now()->addSecond(1))
            );
        }

        if (!is_null($user) && $user->status === 0) {
            $user->update(['status' => 1]);

            $this->telegram->sendMessage(
                $response->message->chat->id,
                'Бот активирован.'
            );
        }
    }
}