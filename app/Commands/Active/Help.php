<?php


namespace App\Commands\Active;

use App\Commands\BaseCommands;
use App\Commands\Command;

class Help extends BaseCommands implements Command
{
    public static $command = '/help';
    public static $description = 'Мой список команд';

    /**
     * @param $response
     * @throws \Exception
     */
    public function runCommand($response)
    {
        $text = '';

        foreach ($this->commands as $command) {
            if ($command::$command !== '/start' && !is_null($command::$command)) {
                $text .= sprintf('%s - %s' . PHP_EOL, $command::$command, $command::$description);
            }
        }

        $this->telegram->sendMessage($response->message->chat->id, $text);
    }
}