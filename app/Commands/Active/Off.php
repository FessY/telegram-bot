<?php

namespace App\Commands\Active;

use App\Commands\BaseCommands;
use App\Commands\Command;
use App\User;

class Off extends BaseCommands implements Command
{
    public static $command = '/off';
    public static $description = 'Выключение';

    /**
     * @param $response
     * @throws \Exception
     */
    public function runCommand($response, User $user = null)
    {
        if (!is_null($user)) {
            $user->update(['status' => 0]);

            $this->telegram->sendMessage($response->message->chat->id, 'Бот отключен.');
        }
    }
}