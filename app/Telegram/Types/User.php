<?php

namespace App\Telegram\Types;

class User extends Type
{
    public $id;
    public $first_name;
    public $last_name;
    public $username;
    public $language_code;
    public $is_bot;

    static protected $map = [
        'id' => true,
        'first_name' => true,
        'last_name' => true,
        'username' => true,
        'language_code' => true,
        'is_bot' => true
    ];
}