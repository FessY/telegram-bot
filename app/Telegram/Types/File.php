<?php

namespace App\Telegram\Types;

class File extends Type
{
    public $file_id;
    public $file_size;
    public $file_path;

    static protected $map = [
        'file_id' => true,
        'file_size' => true,
        'file_path' => true
    ];
}