<?php

namespace App\Telegram\Types;

class Chat extends Type
{
    public $id;
    public $type;
    public $title;
    public $username;
    public $first_name;
    public $last_name;
    public $all_members_are_administrators;
    public $photo;
    public $description;
    public $invite_link;
    public $pinned_message;
    public $sticker_set_name;
    public $can_set_sticker_set;

    static protected $map = [
        'id' => true,
        'type' => true,
        'title' => true,
        'username' => true,
        'first_name' => true,
        'last_name' => true,
        'all_members_are_administrators' => true,
        'photo' => ChatPhoto::class,
        'description' => true,
        'invite_link' => true,
        'pinned_message' => Message::class,
        'sticker_set_name' => true,
        'can_set_sticker_set' => true
    ];
}