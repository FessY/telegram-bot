<?php

namespace App\Telegram\Types;

class ChatPhoto extends Type
{
    public $small_file_id;
    public $big_file_id;

    static protected $map = [
        'small_file_id' => true,
        'big_file_id' => true,
    ];
}