<?php

namespace App\Telegram\Types;

/**
 * Class PhotoSize
 * This object represents one size of a photo or a file / sticker thumbnail.
 *
 * @package TelegramBot\Api\Types
 */
class PhotoSize extends Type
{
    public $file_id;
    public $width;
    public $height;
    public $file_size;

    static protected $map = [
        'file_id' => true,
        'width' => true,
        'height' => true,
        'file_size' => true,
    ];
}
