<?php

namespace App\Telegram\Types;

class Voice extends Type
{
    public $file_id;
    public $duration;
    public $mime_type;
    public $file_size;

    static protected $map = [
        'file_id' => true,
        'duration' => true,
        'mime_type' => true,
        'file_size' => true
    ];
}