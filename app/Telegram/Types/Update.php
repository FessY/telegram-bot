<?php

namespace App\Telegram\Types;

class Update extends Type
{
    public $update_id;
    public $message;
    public $edited_message = Message::class;
    public $channel_post = Message::class;
    public $edited_channel_post = Message::class;
    //public $inline_query = InlineQuery::class;
    //public $chosen_inline_result = ChosenInlineResult::class;
    //public $callback_query = CallbackQuery::class;
    //public $shipping_query = ShippingQuery::class;
    //public $pre_checkout_query = PreCheckoutQuery::class;

    static protected $map = [
        'update_id' => true,
        'message' => Message::class,
        'edited_message' => Message::class,
        'channel_post' => Message::class,
        'edited_channel_post' => Message::class,
        //'inline_query' => InlineQuery::class,
        //'chosen_inline_result' => ChosenInlineResult::class,
        //'callback_query' => CallbackQuery::class,
        //'shipping_query' => ShippingQuery::class,
        //'pre_checkout_query' => PreCheckoutQuery::class,
    ];

    /**
     * @param $data
     * @return array
     */
    public static function fromResponses($data)
    {
        $arrayOfUpdates = [];
        foreach ($data as $update) {
            $arrayOfUpdates[] = self::fromResponse($update);
        }

        return $arrayOfUpdates;
    }
}