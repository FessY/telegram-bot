<?php

namespace App\Telegram\Types;

class Type
{
    protected static $map = [];

    public static function fromResponse($data)
    {
        if ($data === true) {
            return true;
        }

        $instance = new static();

        $instance->map($data);

        return $instance;
    }

    protected function map($data)
    {
        foreach (static::$map as $key => $item) {
            if (isset($data[$key]) && (!is_array($data[$key]) || (is_array($data[$key]) && !empty($data[$key])))) {
                if ($item === true) {
                    $this->$key = $data[$key];
                } else {
                    $this->$key = $item::fromResponse($data[$key]);
                }
            }
        }

        return $this;
    }
}