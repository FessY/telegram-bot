<?php

namespace App\Telegram\Types;

class Message extends Type
{
    public $message_id;
    public $from;
    public $date;
    public $chat;
    public $forward_from;
    public $forward_date;
    public $reply_to_message;
    public $text;
    public $entities;
    public $caption_entities;
    public $audio;
    public $document;
    public $photo;
    public $sticker;
    public $video;
    public $voice;
    public $caption;
    public $contact;
    public $location;
    public $venue;
    public $new_chat_member;
    public $left_chat_member;
    public $new_chat_title;
    public $new_chat_photo;
    public $delete_chat_photo;
    public $group_chat_created;
    public $supergroup_chat_created;
    public $channel_chat_created;
    public $migrate_to_chat_id;
    public $migrate_from_chat_id;
    public $pinned_message;
    public $invoice;
    public $successful_payment;
    public $forward_signature;
    public $author_signature;

    static protected $map = [
        'message_id' => true,
        'from' => User::class,
        'date' => true,
        'chat' => Chat::class,
        'forward_from' => User::class,
        'forward_date' => true,
        'reply_to_message' => Message::class,
        'text' => true,
        //'entities' => ArrayOfMessageEntity::class,
        //'caption_entities' => ArrayOfMessageEntity::class,
        //'audio' => Audio::class,
        //'document' => Document::class,
        'photo' => ArrayOfPhotoSize::class,
        //'sticker' => Sticker::class,
        //'video' => Video::class,
        'voice' => Voice::class,
        'caption' => true,
        //'contact' => Contact::class,
        //'location' => Location::class,
        //'venue' => Venue::class,
        'new_chat_member' => User::class,
        'left_chat_member' => User::class,
        'new_chat_title' => true,
        //'new_chat_photo' => ArrayOfPhotoSize::class,
        'delete_chat_photo' => true,
        'group_chat_created' => true,
        'supergroup_chat_created' => true,
        'channel_chat_created' => true,
        'migrate_to_chat_id' => true,
        'migrate_from_chat_id' => true,
        'pinned_message' => Message::class,
        //'invoice' => Invoice::class,
        //'successful_payment' => SuccessfulPayment::class,
        'forward_signature' => true,
        'author_signature' => true
    ];
}