<?php

namespace App\Telegram;

use App\Telegram\Types\Message;
use App\Telegram\Types\Update;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Telegram
{
    const URL_PREFIX = 'https://api.telegram.org/bot';
    const FILE_URL_PREFIX = 'https://api.telegram.org/file/bot';
    const DEFAULT_STATUS_CODE = 200;
    const NOT_MODIFIED_STATUS_CODE = 304;

    public static $codes = [
        // Informational 1xx
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        // Success 2xx
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        // Redirection 3xx
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found', // 1.1
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        // 306 is deprecated but reserved
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        // Client Error 4xx
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        // Server Error 5xx
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates (Experimental)',                      // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
    ];
    private $token;
    private $curl;

    public function __construct()
    {
        $this->token = env('TELEGRAM_BOT_TOKEN');
        $this->curl = curl_init();
    }

    private function getUrl()
    {
        return self::URL_PREFIX . $this->token;
    }

    /**
     * @param $chatId
     * @param $text
     * @param string $parseMode
     * @return mixed
     * @throws \Exception
     */
    public function sendMessage($chatId, $text, $parseMode = 'html')
    {
        return Message::fromResponse(
            $this->call('sendMessage', [
                    'chat_id' => $chatId,
                    'text' => $text,
                    'parse_mode' => $parseMode,
                ]
            )
        );
    }

    /**
     * @param $chatId
     * @param $photo
     * @param null $caption
     * @return Message
     * @throws \Exception
     */
    public function sendPhoto($chatId, $photo, $caption = null)
    {
        return Message::fromResponse(
            $this->call('sendPhoto', [
                'chat_id' => $chatId,
                'photo' => $photo,
                'caption' => $caption,
            ])
        );
    }

    /**
     * @param $chatId
     * @param $voice
     * @param null $caption
     * @param int $duration
     * @return Message
     * @throws \Exception
     */
    public function sendVoice($chatId, $voice, $caption = null, $duration = 1)
    {
        return Message::fromResponse($this->call('sendVoice', [
            'chat_id' => $chatId,
            'voice' => $voice,
            'caption' => $caption,
            'duration' => $duration,
        ]));
    }

    /**
     * @param $chatId
     * @param $action
     * @return mixed
     * @throws \Exception
     */
    public function sendChatAction($chatId, $action)
    {
        return $this->call('sendChatAction', [
            'chat_id' => $chatId,
            'action' => $action,
        ]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getUpdates()
    {
        return Update::fromResponses($this->call('getUpdates'));
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        if ($data = self::jsonValidate($this->getRawBody())) {
            return Update::fromResponse($data);
        }

        return null;
    }

    private function getRawBody()
    {
        return file_get_contents('php://input');
    }

    /**
     * @param $jsonString
     * @return mixed
     * @throws \Exception
     */
    public static function jsonValidate($jsonString)
    {
        $json = json_decode($jsonString, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            throw new \Exception(json_last_error_msg(), json_last_error());
        }

        return $json;
    }

    /**
     * @param $method
     * @param array|null $data
     * @return mixed
     * @throws \Exception
     */
    public function call($method, array $data = null)
    {
        $options = [
            CURLOPT_URL => $this->getUrl() . '/' . $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => null,
            CURLOPT_POSTFIELDS => null,
        ];

        if ($data) {
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = $data;
        }

        $response = self::jsonValidate($this->executeCurl($options));

        if (!isset($response['ok'])) {
            throw new HttpException($response['description'], $response['error_code']);
        }

        return $response['result'];
    }

    /**
     * @param array $options
     * @return mixed
     * @throws \Exception
     */
    protected function executeCurl(array $options)
    {
        curl_setopt_array($this->curl, $options);

        $result = curl_exec($this->curl);
        self::curlValidate($this->curl, $result);
        if ($result === false) {
            throw new \Exception(curl_error($this->curl), curl_errno($this->curl));
        }

        return $result;
    }

    /**
     * @param $curl
     * @param null $response
     * @throws \Exception
     */
    public static function curlValidate($curl, $response = null)
    {
        $json = json_decode($response, true) ?: [];
        if (($httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE))
            && !in_array($httpCode, [self::DEFAULT_STATUS_CODE, self::NOT_MODIFIED_STATUS_CODE])
        ) {
            $errorDescription = array_key_exists('description', $json) ? $json['description'] : self::$codes[$httpCode];
            //$errorParameters = array_key_exists('parameters', $json) ? $json['parameters'] : [];
            throw new \Exception($errorDescription, $httpCode, null);
        }
    }
}