<?php

namespace App;

use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Word
 *
 * @property string name
 * @property string transcription
 * @property string translation
 * @property string last_word
 * @property Audio audio
 * @property Image image
 */
class Word extends Model
{
    CONST WORD_ID = 'word_id';
    CONST RANDOM_WORD_ID = 'random_word_id';

    protected $table = 'words';

    protected $fillable = [
        'name',
        'transcription',
        'translation',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(Image::class, 'word_id');
    }

    public function audio()
    {
        return $this->hasOne(Audio::class, 'word_id');
    }

    /**
     * @return Word
     */
    public static function createWord()
    {
        $wordId = Cache::get(self::WORD_ID);

        if (!is_null($wordId)) {
            return Word::find($wordId);
        }

        $word = self::createRandomWord();

        $expiresAt = Carbon::now()->addHours(22);

        Cache::put(self::WORD_ID, $word->id, $expiresAt);

        return $word;
    }

    /**
     * @return Word
     */
    public static function createRandomWord()
    {
        $wordCount = self::count();

        do {
            $random = rand(1, $wordCount);
        } while (Cache::get(self::RANDOM_WORD_ID, 0) === $random || Cache::get(self::WORD_ID, 0) === $random);

        Cache::forever(self::RANDOM_WORD_ID, $random);

        return Word::find($random);
    }
}