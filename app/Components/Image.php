<?php

namespace App\Components;

use App\Telegram\Types\Message;
use App\Word;

class Image
{
    private $word;

    private $files;

    private $photoUrl;

    private $photoPath;

    /**
     * @param Word $word
     * @return string
     */
    public function make(Word $word)
    {
        $this->word = $word;

        if (!is_null($this->word->image)) {
            return $this->word->image->file_id;
        }

        $imageName = $this->word->name . '_' . str_limit(md5($this->word->name . str_random()), 5, '') . '.png';
        $this->files = $this->getBackground();
        $this->photoUrl = env('APP_DOMEN') . '/' . $imageName;
        $this->photoPath = base_path('public/') . $imageName;

        $this->createImage();

        return $this->checkPhoto() ? $this->getData() : null;
    }

    /**
     * Create image
     *
     * @return bool
     */
    private function createImage()
    {
        if (count($this->files) === 0) {
            return false;
        }

        $rand = array_rand($this->files);
        $img = $this->files[$rand];

        $imgUrl = $this->getStorageImagesPath() . '/' . $img;

        $font = storage_path('app/fonts') . '/hagin.OTF';
        $font2 = storage_path('app/fonts') . '/times.ttf';

        $fontSize = 50;

        $name = $this->word->name;
        $transcription = '[' . $this->word->transcription . ']';
        $translation = $this->word->translation;

        $pic = imagecreatefrompng($imgUrl); //открываем рисунок

        $white = imagecolorallocate($pic, 255, 255, 255);

        // crop
        //$size = min(imagesx($pic), imagesy($pic));
        //$pic = imagecrop($pic, ['x' => 0, 'y' => 0, 'width' => $size, 'height' => $size]);

        // вычисляем сколько места займёт текст
        $bbox = imageftbbox($fontSize, 0, $font, $name);
        $bbox2 = imageftbbox($fontSize, 0, $font2, $transcription);
        $bbox3 = imageftbbox($fontSize, 0, $font, $translation);

        // вычисляем координаты для центрирования
        $w = (imagesx($pic) - $bbox[4]) / 2;
        $h = (imagesy($pic) - $bbox[5]) / 2 - 110;

        $w2 = (imagesx($pic) - $bbox2[4]) / 2;
        $h2 = (imagesy($pic) - $bbox2[5]) / 2 - 40;

        $w3 = (imagesx($pic) - $bbox3[4]) / 2;
        $h3 = (imagesy($pic) - $bbox3[5]) / 2 + 60;

        /* выводим текст на изображение */
        imagettftext($pic, $fontSize, 0, $w, $h, $white, $font, $name);
        imagettftext($pic, $fontSize, 0, $w2, $h2, $white, $font2, $transcription);
        imagettftext($pic, $fontSize, 0, $w3, $h3, $white, $font, $translation);

        $finish = imagepng($pic, $this->photoPath); //сохраняем рисунок
        imagedestroy($pic);

        return $finish;
    }

    /**
     * @return bool
     */
    public function checkPhoto()
    {
        return file_exists($this->photoPath);
    }

    /**
     * @return string
     */
    public function getData()
    {
        //$photo = Cache::has('lastPhotoID') ? Cache::get('lastPhotoID') : $this->photoUrl;
        //var_dump($photo);
        //return $photo;
        return $this->photoUrl;
    }

    private function getBackground()
    {
        return array_diff(scandir($this->getStorageImagesPath()), ['..', '.']);
    }

    private function getStorageImagesPath()
    {
        return storage_path('app/images');
    }

    /**
     * @param Message $message
     */
    public function save(Message $message)
    {
        if (is_null($this->word->image)) {
            $count = count($message->photo) - 1;

            $this->word->image()->create(['file_id' => $message->photo[$count]->file_id]);

            unlink($this->photoPath);
        }
    }
}