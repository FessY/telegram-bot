<?php

namespace App\Components\Audio;

use App\Telegram\Types\Message;
use App\Word;

class Audio
{
    private $create;
    private $word;

    public function __construct(Create $create)
    {
        $this->create = $create;
    }

    /**
     * @param Word $word
     * @return Create
     */
    public function make(Word $word)
    {
        $this->word = $word;

        try {
            return $this->create->make($word);
        } catch (\Exception $e) {
            \Log::warning($e);

            return null;
        }
    }

    /**
     * @param Message $message
     */
    public function save(Message $message)
    {
        if (is_null($this->word->audio)) {
            $this->word->audio()->create(['file_id' => $message->voice->file_id]);
        }
    }
}