<?php

namespace App\Components\Audio;

use Exception;
use App\Word;
use Log;

class Create
{
    private $word;
    private $audioUrl;
    private $audioId;
    private $audioNameMP3;
    private $audioNameOGG;
    private $audioPathMP3;
    private $audioPathOGG;
    //private $durationAudio;

    /**
     * @param Word $word
     * @return string
     */
    public function make(Word $word)
    {
        $this->word = $word;
        $this->audioId = $word->audio ? $word->audio->file_id : null;
        $this->audioNameMP3 = $this->word->name . '.mp3';
        $this->audioNameOGG = $this->word->name . '.ogg';
        $this->audioUrl = env('APP_DOMEN') . '/storage/audio/' . $this->audioNameOGG;
        $this->audioPathMP3 = storage_path('app/public/audio/') . $this->audioNameMP3;
        $this->audioPathOGG = storage_path('app/public/audio/') . $this->audioNameOGG;

        if (!$this->checkAudio()) {
            $this->createAudio();
        }

        return $this->getData();
    }

    /**
     * Create audio
     */
    public function createAudio()
    {
        try {
            $word = strtolower($this->word->name);

            try {
                $url = env('AUDIO_SITE_URL') . $word;
                $opts = [
                    'http' => [
                        'method' => "GET",
                        'header' => "Accept: application/json\r\n" .
                            "app_id: " . env('AUDIO_SITE_URL_APP_ID') . "\r\n" .
                            "app_key: " . env('AUDIO_SITE_URL_APP_KEY') . "\r\n"
                    ]
                ];
                $context = stream_context_create($opts);

                $this->downloadAudio($url, $context);
            } catch (Exception $e) {
                Log::warning("Error of creating audio in AUDIO_SITE_URL. Word: {$this->word->name}. File: {$e->getFile()}; Line: {$e->getLine()}; Message: {$e->getMessage()}");

                //https://cache-a.oddcast.com/c_fs/d95fb426dd6e1adf37e31008e0604bd2.mp3?engine=3&language=1&voice=3&text=say&useUTF8=1
                // A Choice of voice - 301004 => 3....4 => 3 - engineID, 4 - voiceID
                $engineID = 3;
                $voiceID = 4;
                $langID = 1;
                $path = "<engineID>{$engineID}</engineID><voiceID>{$voiceID}</voiceID><langID>{$langID}</langID><ext>mp3</ext>{$word}";
                $url = env('AUDIO_SITE_URL_ODDCAST') .
                    md5($path) . ".mp3" .
                    "?engine={$engineID}" .
                    "&voice={$voiceID}" .
                    "&language={$langID}" .
                    "&text={$word}" .
                    "&useUTF8=1";

                $this->downloadAudio($url);
            }

            $this->convertToOgg();
        } catch (Exception $e) {
            Log::warning("Error of creating audio. Word: {$this->word->name}. File: {$e->getFile()}; Line: {$e->getLine()}; Message: {$e->getMessage()}");
        }
    }

    /**
     * @return bool
     */
    public function checkAudio()
    {
        return file_exists($this->audioPathOGG);
    }

    /**
     * @return string
     */
    public function getData()
    {
        //$audio = Cache::has('lastAudioID') ? Cache::get('lastAudioID') : $this->audioUrl;
        //return $audio;

        return $this->audioId ?? $this->audioUrl;
    }

//
//    /**
//     * @return int
//     */
//    public function getDurationAudio()
//    {
//        return $this->durationAudio;
//    }

    /**
     * Set duration of audio
     */
//    public function setDurationAudio()
//    {
//        $mp3Helper = new MP3Helper($this->audioPathMP3);
//        $duration = $mp3Helper->getDurationEstimate();
//        $result = exec('ffmpeg -i ' . $this->audioPathMP3 . ' 2>&1 | grep Duration');
//        preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result, $match);
//        dd($result, $match);
//        $this->durationAudio = (int)round($duration);
//    }

    /**
     * @param string $url
     * @param null $context
     */
    private function downloadAudio(string $url, $context = null)
    {
        if (strpos($url, env('AUDIO_SITE_URL')) !== false) {
            $data = file_get_contents($url, false, $context);

            $data = json_decode($data);
            $data = $data->results[0]->lexicalEntries[0]->pronunciations[0]->audioFile;
        } else {
            $data = $url;
        }

        $audio = file_get_contents($data);
        file_put_contents($this->audioPathMP3, $audio);
    }

    private function convertToOgg()
    {
        exec("ffmpeg -y -i {$this->audioPathMP3} -acodec opus -b:a 128k {$this->audioPathOGG}");

        unlink($this->audioPathMP3);
    }
}
