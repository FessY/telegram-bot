<?php

namespace App\Components;

use App\Word;

class Text
{
    public function make(Word $word)
    {
        return "<b>{$word->name}</b> <i>[{$word->transcription}]</i> - {$word->translation}";
    }
}