<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{
    protected $table = "audio";

    protected $fillable = [
        'word_id',
        'file_id',
    ];
}