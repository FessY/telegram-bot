<?php

namespace App\Http\Controllers;

use App\Commands\BaseCommands;
use App\Commands\BaseEventCommands;
use App\Components\Audio\Audio;
use App\Components\CreateAudio;
use App\Components\Image;
use App\Components\Text;
use App\Jobs\SendMessage;
use App\Telegram\Telegram;
use App\User;
use App\Word;
use Carbon\Carbon;
use Queue;

class BaseController extends Controller
{
    /**
     * @var Telegram
     */
    private $telegram;
    /**
     * @var BaseCommands
     */
    private $commands;
    /**
     * @var BaseEventCommands
     */
    private $eventCommands;

    public function __construct(Telegram $telegram, BaseCommands $commands, BaseEventCommands $eventCommands)
    {
        $this->telegram = $telegram;
        $this->commands = $commands;
        $this->eventCommands = $eventCommands;
    }

    public function setWebHook()
    {

    }

    /**
     * @throws \Exception
     */
    public function index()
    {
//        $responses = $this->telegram->getUpdates();
//
//        // Last array for getUpdates
//        if (count($responses) > 0) {
//            $response = $responses[count($responses) - 1];
//        }

        $response = $this->telegram->run();

        if (!isset($response->message) || is_null($response->message)) {
            exit;
        }

        $user = User::where('chat_id', $response->message->chat->id)->first();

        $this->commands->runCommands($response, $user);

        $this->eventCommands->runCommands($response, $user);
    }

    /**
     * @throws \Throwable
     */
    public function test()
    {
        Queue::push(
            (new SendMessage(User::first()))->delay(Carbon::now()->addSecond(1))
        );
    }
}
