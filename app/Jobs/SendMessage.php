<?php

namespace App\Jobs;

use App\Components\Audio\Audio;
use App\Components\Text;
use App\Components\Image;
use App\Telegram\Telegram;
use Exception;
use App\User;
use App\Word;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SendMessage extends Job
{
    private $user;
    private $randomWord;

    /**
     * SendMessage constructor.
     * @param User $user
     * @param bool $randomWord
     */
    public function __construct(User $user, $randomWord = false)
    {
        $this->user = $user;
        $this->randomWord = $randomWord;
    }

    /**
     * @param Telegram $telegram
     * @param Image $image
     * @param Text $text
     * @param Audio $audio
     */
    public function handle(Telegram $telegram, Image $image, Text $text, Audio $audio)
    {
        try {
            $word = $this->randomWord ? Word::createRandomWord() : Word::createWord();

            if ($this->user->chat_id === 0) {
                $this->user->update(['status' => 0]);

                throw new Exception('Некорректный chat_id = 0. User ID = ' . $this->user->id);
            }

            if ($imagePath = $image->make($word)) {
                $telegram->sendChatAction($this->user->chat_id, 'upload_photo');

                $image->save(
                    $telegram->sendPhoto($this->user->chat_id, $imagePath)
                );
            } else {
                $telegram->sendMessage($this->user->chat_id, $text->make($word));
            }

            if ($audioUrl = $audio->make($word)) {
                $telegram->sendChatAction($this->user->chat_id, 'upload_audio');

                $audio->save(
                    $telegram->sendVoice($this->user->chat_id, $audioUrl, $word->name)
                );
            }

            Log::info("Word was sent {$this->user->username} {$this->user->first_name} {$this->user->last_name}");
        } catch (HttpException $e) {
            if ($e->getHttpStatusCode() === 403) {
                Log::warning("Bot was blocked a user. Chat: " . $this->user->chat_id . "; First name: " . $this->user->first_name);
                $this->user->update(['status' => 0]);
            } else {
                Log::warning($e);
            }
        } catch (Exception $e) {
            Log::warning($e->getMessage());
        }
    }
}
