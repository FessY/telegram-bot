<?php

namespace App\Console\Commands;

use App\Jobs\SendMessage;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Queue\SerializesModels;
use Log;
use Queue;

class sendWordCommand extends Command
{
    use SerializesModels;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:word';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a word';

    /**
     * sendWordCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle()
    {
        $timeStart = microtime(true);

        Log::info("--- Begin send:word ---");

        User::chunk(20, function ($users) {
            foreach ($users as $user) {
                // With 6 until 12 hours - not to send
                if ($user->status === 1) {
                    Queue::push(
                        (new SendMessage($user))->delay(Carbon::now()->addSecond(1))
                    );
                }
            }
        });

        $timeEnd = microtime(true);
        $time = $timeEnd - $timeStart;

        Log::info("--- End send:word time: {$time} с. ---\n");

        return true;
    }
}