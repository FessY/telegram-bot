<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 *
 * @property string username
 * @property string first_name
 * @property string last_name
 * @property integer user_id
 * @property integer chat_id
 * @property boolean status
 */
class User extends Model
{
    protected $table = "users";

    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'user_id',
        'chat_id',
        'status',
    ];

    public function logging()
    {
        return $this->hasMany(Log::class, 'user_id', 'user_id');
    }
}
