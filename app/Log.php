<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 *
 * @property integer user_id
 * @property integer update_id
 * @property integer message_id
 * @property string text
 * @property string date
 */
class Log extends Model
{

    protected $table = "logs";

    protected $fillable = [
        'user_id',
        'update_id',
        'message_id',
        'text',
        'date',
    ];

    protected $dates = [
        'date',
    ];

    public $timestamps = false;
}