<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Temp
 * @package App\Models
 *
 * @property integer word_id
 */
class Temp extends Model
{
    protected $table = "temps";

    protected $fillable = [
        'word_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function word()
    {
        return $this->belongsTo(Word::class, 'word_id');
    }
}